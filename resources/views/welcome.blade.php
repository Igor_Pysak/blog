@extends('layouts.layout')
@section('title')Головна сторінка@endsection
@section ('content')
<div class="row py-lg-5">
    <div class="col-lg-6 col-md-8 mx-auto text-white">
        <main class="px-3" align="center">
            <h1>Статьи</h1>
        </main>
    </div>
        <div class="container">
            <div class="row">
                @foreach ($posts_bydate as $post)
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow bg-dark">
                    <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" src="{{ $post['img'] }}" data-holder-rendered="true" style="height: 225px; width: 100%; display: block;">
                    <div class="card-body">
                        <p class="card-text text-white">{{ $post->title }}</p>
                        <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                        <a class="btn btn-sm btn-outline-info" href="{{ route('page', $post->id) }}">Посмотреть</a>
                    </div>
                    <p class="card-text text-white">{{ $post->created_at }}</p>
                    </div>
                </div>
            </div>
        </div>
      
        @endforeach
            {{ $posts_bydate->links() }}
</div>
@endsection
