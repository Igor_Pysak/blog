@extends('layouts.layout')
@section('title'){{ $post['title'] }}@endsection
@section ('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item text-white"><a href="/">Главная</a></li>
      <li class="breadcrumb-item active text-white" aria-current="page">Статья - {{ $post['title'] }}</li>
    </ol>
  </nav>
    <div class="row">
        <header class="entry-header">
            <img src="/{{ $post['img'] }}" width="1140" height="652" alt="" loading="lazy" sizes="(max-width: 1140px) 100vw, 1140px"></div>
            <div class="mx-auto" style="width: 300px;">
            <time class="entry-date published text-white" datetime="">Опубликовано в - {{ $post['created_at'] }}</time>
            </div>
            <h1 class="mx-3 py-3 entry-title text-white">{{ $post['title'] }}</h1>
        </header>
        <div class="entry-content text-white">
            <p>{{ $post['text'] }}</p>
    </div>
  <nav aria-label="Page navigation example">
    <ul class="pagination">
      @isset($previous_page->id)
        <li class="page-item"><a class="page-link" href="{{ route('page', $previous_page->id) }}">Previous</a></li>
      @endisset
      @isset($next_page->id)
        <li class="page-item"><a class="page-link" href="{{ route('page', $next_page->id) }}">Next</a>
      @endisset
    </ul>
  </nav>
  @endsection
