<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $status = Post::select('status')->get();
        $posts_bydate = Post::where('status', 1)->orderBy('created_at', 'DESC')->paginate(6);
        return view('welcome', compact('posts_bydate', 'status'));
    }
}
