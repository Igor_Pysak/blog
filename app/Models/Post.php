<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentTaggable\Taggable;

class Post extends Model
{
    use HasFactory;
    use Taggable;

    public function category()
    {
        return $this->belongsTo(Category::class, 'cat_id');
    }
}
